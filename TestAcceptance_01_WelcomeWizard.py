import pytest

from AbstractTest import AbstractTest

step = pytest.allure.step


class TestAcceptanceWelcomeWizard(AbstractTest):

    def tess_run_app(self):
        """
        Run application on device and check that it opened
        """
        self._run_app()
        assert self.current_app == self.app_info['name']

    def test_pass_welcome_wizard(self):
        """
        Pass store welcome wizard with check prev/next actions in slider
        """

        self.open_welcome_wizard_screen('Home', 'next')
        self.open_welcome_wizard_screen('My Games', 'next')
        self.open_welcome_wizard_screen('All Games', 'prev')
        self.open_welcome_wizard_screen('My Games', 'next')
        self.open_welcome_wizard_screen('All Games', 'next')

        start_usage_btn = self.d.ui(resourceId=self.app_info['name'] + ':id/start')
        start_usage_btn.click.wait()

        with step("Check that on %s home screen" % self.app_info['name']):
            assert 'PlayFree Games' in self.store_screen.top_bar_title

    def test_close_app(self):
        """
        Shutdown application and check operation status
        """
        self._run_app()
        self._close_app()
        with step("Check that application closed"):
            assert self.d.ui.info['currentPackageName'] != self.app_info['name']
