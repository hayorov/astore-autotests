import pytest

from AbstractTest import AbstractTest

step = pytest.allure.step


class TestAcceptanceInstallAndRun(AbstractTest):

    def test_install_app(self):
        """
        Install Store APK to target device and check that present in system
        """
        with step("Installing APK"):
            self.d.install_app(self.app_info['apk_path'])
        with step("Check that application installed properly"):
            assert self.app_info['name'] in self.d.system_app_list

    def test_run_app(self):
        """
        Run application on device and check that it opened
        """
        self._run_app()
        assert self.current_app == self.app_info['name']

    def test_store_ico_created_on_launcher(self):
        """
        Check that store ico created on android launcher after installation
        """
        self.open_android_launcher()
        assert self.store_launcher_ico

    def test_run_store_from_launcher(self):
        """
        Run store from android launcher ico
        """
        self.open_android_launcher()
        self.store_launcher_ico.click.wait()
        assert self.current_app == self.app_info['name']

    def test_close_app(self):
        """
        Shutdown application and check operation status
        """
        self._run_app()
        self._close_app()
        with step("Check that application closed"):
            assert self.d.ui.info['currentPackageName'] != self.app_info['name']
