import pytest

from AbstractTest import AbstractTest

step = pytest.allure.step


class TestAcceptanceTabs(AbstractTest):

    def _test_home_tab_simple(self):
        """
        Open home tab and check important elements is present
        """
        self.open_store_home_screen()
        assert self.d.ui().child_by_text('POPULAR', allow_scroll_search=True).exists
        assert self.d.ui().child_by_text('EDITOR\'S CHOICE', allow_scroll_search=True).exists
        assert self.d.ui().child_by_text('NEW GAMES', allow_scroll_search=True).exists

    def _test_my_games_tab_simple(self):
        """
        Open hmy games tab and check important elements is present
        """
        self.open_store_home_screen()
        self.store_screen.action_bar.my_games.click.wait()
        assert self.d.ui().child_by_text('FIND GAMES', allow_scroll_search=True).exists

    def _test_all_games_tab_simple(self):
        """
        Open all games tab and check important elements is present
        """
        self.open_store_home_screen()
        self.store_screen.action_bar.all_games.click.wait()
        assert self.d.ui().child_by_text('FREE', allow_scroll_search=True).exists

    def test_open_terms_of_use(self):
        """
        Open terms of use
        """
        self.open_store_home_screen()
        terms_of_use = self.d.ui().child_by_text('Terms of Use', allow_scroll_search=True)
        assert terms_of_use.exists, "Terms of use text expected"
        terms_of_use_btn = self.d.ui(resourceId='%s:id/license3' % self.store_screen.package_name)
        assert terms_of_use_btn.exists
        terms_of_use_btn.click.wait()
        assert self.current_app == 'com.android.browser'

    def test_settings_page_simple(self):
        """
        Open settings page and check important elements is present
        """
        self.open_store_home_screen()
        self.store_screen.top_bar_more_options.click.wait()
        self.d.ui(text='SETTINGS').click.wait()
        assert self.d.ui().child_by_text('DOWNLOADING BY 3G', allow_scroll_search=True).exists
        assert self.d.ui().child_by_text('PUSH NOTIFICATIONS', allow_scroll_search=True).exists
        assert self.d.ui().child_by_text('SIDE MENU', allow_scroll_search=True).exists

    def test_about_page_simple(self):
        """
        Open about page and check important elements is present
        """
        self.open_store_home_screen()
        self.store_screen.top_bar_more_options.click.wait()
        self.d.ui(text='ABOUT').click.wait()
        assert self.d.ui(scrollable=True).fling.toEnd(), "Expecting scrollable element"
        assert self.d.ui(textContains='ABOUT').exists

    def test_feedback_page_simple(self):
        """
        Open feedback page and check important elements is present
        """
        self.open_store_home_screen()
        self.store_screen.top_bar_more_options.click.wait()
        self.d.ui(text='SEND FEEDBACK').click.wait()
        assert self.d.ui(textContains='SEND FEEDBACK').exists

    def test_open_first_popular_game(self):
        """
        Click on first game from section 'Popular' and validate game details screen
        """
        self.open_store_home_screen()
        self.store_screen.action_bar.home.click.wait()
        self.store_home_screen.popular_first()
        self.check_game_details_screen()

    def test_open_first_editors_choice_game(self):
        """
        Click on first game from section 'editors_choice' and validate game details screen
        """
        self.open_store_home_screen()
        self.store_screen.action_bar.home.click.wait()
        self.store_home_screen.editors_choice_first()
        self.check_game_details_screen()

    def test_open_first_new_games_game(self):
        """
        Click on first game from section 'new_games' and validate game details screen
        """
        self.open_store_home_screen()
        self.store_screen.action_bar.home.click.wait()
        self.store_home_screen.new_games_first()
        self.check_game_details_screen()

    def test_close_app(self):
        """
        Shutdown application and check operation status
        """
        self._run_app()
        self._close_app()
        with step("Check that application closed"):
            assert self.d.ui.info['currentPackageName'] != self.app_info['name']
