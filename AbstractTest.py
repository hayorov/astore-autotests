import pytest

from Lib.CommonActions import CommonActions
from Lib.DeviceEmulator import DeviceEmulator
from Lib.ApkInfo import apk_info
from Lib.Helpers import get_apk_from_ci

AVD = 'etalon-18-debug'  # etalon-18, etalon-17
BUILD = '1.2-86'

step = pytest.allure.step


class AbstractTest(CommonActions):

    d = None
    app_info = {}
    
    def analyze_app(self, build):
        self.app_info['apk_path'] = get_apk_from_ci(build)
        self.app_info.update({
            'name': apk_info(self.app_info['apk_path'])['name'],
            'version': apk_info(self.app_info['apk_path'])['versionName'],
            'start_activity': apk_info(self.app_info['apk_path'])['activity'], }
        )

    def setup(self):
        self.analyze_app(BUILD)
        self.d = DeviceEmulator(AVD, False)

    def _run_app(self):
        """
        Run application on device
        """
        with step("Run %s " % self.app_info['name']):
            self.open_store_home_screen()

    def _close_app(self):
        """
        Shutdown application
        """
        with step("Close %s" % self.app_info['name']):
            self.d.stop_app(self.app_info['name'])

    def teardown(self):
        #self.d.stop_device()
        pass
