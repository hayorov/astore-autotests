#!/bin/sh
Xvfb :99 -ac &
x11vnc -dontdisconnect -noxfixes -shared -forever -rfbport 5900 -bg -o /dev/null -display :99 -ncache 10 -noxdamage &