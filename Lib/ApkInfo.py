# Copyright (c) 2014 Alexander Hayorov <i@hayorov.ru>
import os
from subprocess import Popen, PIPE

if not 'AAPT_PATH' in os.environ:
    AAPT_PATH = ''
else:
    AAPT_PATH = os.environ['AAPT_PATH']


def apk_info(apk_path):

    def split_e(e):
        return map(lambda x: x.replace("'", ''), e.split('='))

    cmd = [os.path.join(AAPT_PATH, 'aapt'), 'dump', 'badging', apk_path]
    #print("[DEBUG] CMD:%s" % " ".join(cmd))
    proc = Popen(cmd, stdout=PIPE, stderr=PIPE)
    stdout, stderr = proc.communicate()
    rc = proc.returncode
    if rc:
        raise Exception("Execution failed. rc=%s, stdout=%s ,stderr=%s" % (rc, stdout, stderr))
    raw_info = stdout.split("\n")
    info = {}
    for line in raw_info:
        if line.startswith('package: '):
            for e in line.replace('package: ', '').split(' '):
                info.update({split_e(e)[0]: split_e(e)[1]})
        if line.startswith('launchable-activity: '):
            info.update({'activity': split_e(line.split(" ")[1])[1]})
    return info
