# Copyright (c) 2014 Alexander Hayorov <i@hayorov.ru>
import requests
import os
from tempfile import gettempdir


def get_apk_from_ci(build):
    url = 'http://d.2apk.ru:8080/Store_%s.apk' % build
    target = os.path.join(gettempdir(), os.path.basename(url))
    r = requests.get(url, stream=True)
    with open(target, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk:  # filter out keep-alive new chunks
                f.write(chunk)
                f.flush()
    return target
