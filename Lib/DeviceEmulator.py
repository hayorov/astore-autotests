# Copyright (c) 2014 Alexander Hayorov <i@hayorov.ru>
import os
import time
from uiautomator import Device
from subprocess import Popen, call, PIPE

if not 'ANDROID_HOME' in os.environ:
    ANDROUD_HOME = '/Applications/AndroidStudio.app/sdk'
else:
    ANDROUD_HOME = os.environ['ANDROID_HOME']

ADB_PATH = os.path.join(ANDROUD_HOME, 'platform-tools/adb')
EMULATOR_PATH = os.path.join(ANDROUD_HOME, 'tools/emulator')


class DeviceEmulator:

    emulator_pid = None
    _automator = None
    display = None

    def __init__(self, avd_name, wipe=False):
        self.avd_name = avd_name
        if self.state == 'unknown':
            print("Starting device from avd `%s`" % avd_name)
            self.start_device(avd_name, self.port, wipe)
        else:
            print("Device %s already running" % self.name)
            if wipe:
                self.wipe()
        self.wait_for_device()
        if not self.state == 'device':
            raise Exception("Unable to start device %s" % self.name)
        self.wait_for_device()

    @property
    def port(self):
        if self.avd_name == 'etalon-18':
            return '5554'
        if self.avd_name == 'etalon-18-debug':
            return '5554'
        else:
            raise Exception("Define port for device %s" % self.avd_name)

    @property
    def ui(self):
        if not self._automator:
            self._automator = Device(self.name)
        return self._automator

    @property
    def name(self):
        return 'emulator-%s' % self.port

    def start_device(self, avd_name, port, wipe=False):
        cmd = [EMULATOR_PATH, '@%s' % avd_name, '-port', str(port), ]
        if wipe:
            cmd += ['-wipe-data']
        proc = Popen(cmd, stdout=PIPE, stderr=PIPE, env={'DISPLAY': ':%s.0' % self.display} if self.display else {})
        self.emulator_pid = proc.pid
        print ("Started `emulator-%s` PID=`%s`" % (port, proc.pid))
        time.sleep(10)

    def init_display(self, port):
        display = str(port[-2:])
        print ("Using DISPLAY=%s" % self.display)
        call("ps aux | grep :%s | grep -v grep | awk '{print $2}' | xargs kill -9" % display, shell=True)
        call("Xvfb :%s -ac -screen 0 480x800x24 &" % display, shell=True)
        call("x11vnc -dontdisconnect -noxfixes -shared -forever -rfbport 5900 "
             "-bg -o /dev/null -display :%s.0 -ncache 10 -noxdamage" % display, shell=True)
        return display

    def wait_for_device(self):
        return not self._exec_cmd(['wait-for-device'])['rc']

    def install_app(self, apk_path='/Users/allexx/Downloads/PlayFreeGames.apk'):
        r = self._exec_cmd(['install', apk_path])
        if not 'Success' in r['stdout']:
            raise Exception("Operation failed. rc=%(rc)s, stdout=%(stdout)s, stderr=%(stderr)s" % r)

    def uninstall_app(self, package_name='com.myplaycity.store'):
        r = self._exec_cmd(['uninstall', package_name])
        if not 'Success' in r['stdout']:
            raise Exception("Operation failed. rc=%(rc)s, stdout=%(stdout)s, stderr=%(stderr)s" % r)

    def _exec_cmd(self, args):
        if not self.name:
            raise Exception("Device have not name")
        cmd = [ADB_PATH, '-s', self.name, ] + args
        #print("[DEBUG] CMD:%s" % " ".join(cmd))
        proc = Popen(cmd, stdout=PIPE, stderr=PIPE)
        stdout, stderr = proc.communicate()
        rc = proc.returncode
        if rc:
            raise Exception("Execution failed. rc=%s, stdout=%s ,stderr=%s" % (rc, stdout, stderr))
        return {'rc': rc, 'stdout': stdout, 'stderr': stderr}

    def cmd(self, args):
        return self._exec_cmd(['shell', ] + args)

    def run_app(self, app_name='com.myplaycity.store', activity='app.home.HomeActivity'):
        if not activity.startswith(app_name):
            activity = "%s.%s" % (app_name, activity)
        return not self.cmd(['am', 'start', '-n', '%s/%s' % (app_name, activity)])['rc']

    def stop_app(self, app_name='com.myplaycity.store'):
        return not self.cmd(['am', 'force-stop', app_name, ])['rc']

    @property
    def state(self):
        r = self._exec_cmd(['get-state'])['stdout'].strip()
        if r not in ['device', 'unknown', 'bootloader']:
            raise Exception("Unexpected device %s state %s" % (self.name, r))
        return r

    def stop_device(self):
        return not self._exec_cmd(['emu', 'kill'])

    @property
    def system_app_list(self):
        return filter(lambda x: x, map(lambda x: x.strip(),
                      self.cmd(['pm', 'list', 'packages'])['stdout'].replace('package:', '').split("\n")))

    def wipe(self):
        self.stop_device()
        self.start_device(self.avd_name, self.port, wipe=True)
