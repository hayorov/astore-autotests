# Copyright (c) 2014 Alexander Hayorov <i@hayorov.ru>
from abc import ABCMeta, abstractproperty
import pytest

from Lib.CommonObjects import CommonObjects

step = pytest.allure.step


class CommonActions(CommonObjects):
    __metaclass__ = ABCMeta

    @abstractproperty
    def d(self):
        pass

    @abstractproperty
    def app_info(self):
        pass

    def open_android_launcher(self):
        '''
        Open default android launcher and validate
        '''
        with step("Open android launcher"):
            self.d.ui.press.home()
            assert self.current_app == 'com.android.launcher'

    def open_store_home_screen(self):
        with step("Open acclication home screen"):
            self.d.stop_app(self.app_info['name'])
            with step("Run %s/%s" % (self.app_info['name'], self.app_info['start_activity'])):
                self.d.run_app(self.app_info['name'], self.app_info['start_activity'])
                assert self.current_app == self.app_info['name']

    def open_welcome_wizard_screen(self, name, action=None):
        with step("Check that on wizard screen %s" % name):
            label = self.d.ui(text=name)
            assert label.text == name
            if action:
                with step("Do action %s" % action):
                    next_btn = self.d.ui(resourceId=self.app_info['name'] + ':id/%s' % action)
                    next_btn.click.wait()

    def check_game_details_screen(self):
        with step("Check game details screen"):
            assert self.store_screen.top_bar_title == 'GAME'
            assert self.d.ui(className='android.widget.Button').text in ['DOWNLOAD', 'INSTALL', ]

            self.d.ui(scrollable=True).scroll.to(text="TELL YOUR FRIENDS")

            # ABOUT Section
            self.d.ui(scrollable=True).scroll.to(text="MORE")
            self.d.ui(text='MORE').click.wait()
            self.d.ui(scrollable=True).scroll.to(text="LESS")
            self.d.ui(text='LESS').click.wait()
            self.d.ui(scrollable=True).scroll.to(text="MORE")

            self.d.ui(scrollable=True).scroll.to(text="POPULAR")

            self.d.ui(scrollable=True).scroll.to(text="MORE BY DEVELOPER")

            self.d.ui(scrollable=True).scroll.to(text='PERMISSIONS')
