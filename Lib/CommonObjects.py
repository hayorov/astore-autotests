# Copyright (c) 2014 Alexander Hayorov <i@hayorov.ru>
from abc import ABCMeta, abstractproperty
import pytest

step = pytest.allure.step


class CommonObjects(object):
    __metaclass__ = ABCMeta

    @abstractproperty
    def d(self):
        pass

    @property
    def store_launcher_ico(self):
        return self.d.ui(textContains='PlayFree')

    @property
    def current_app(self):
        return self.d.ui.info['currentPackageName']

    @property
    def store_screen(self):
        return StoreScreen(self.d)

    @property
    def store_home_screen(self):
        return StoreHomeScreen(self.d)


class StoreBase(object):
    package_name = 'com.myplaycity.store'

    def __init__(self, d):
        self.d = d


class StoreScreen(StoreBase):

    @property
    def top_bar_title(self):
        with step("Top bar title"):
            return self.d.ui(packageName=self.package_name, resourceId='android:id/action_bar_title').text

    @property
    def top_bar_more_options(self):
        with step("Top bar options"):
            return self.d.ui(packageName=self.package_name, description='More options')

    @property
    def action_bar(self):
        with step("Action bar"):
            return StoreActionBar(self.d)


class StoreActionBar(StoreBase):

    class_name = 'ActionBar'

    @property
    def home(self):
        with step("Go HOME tab"):
            return self.d.ui(packageName=self.package_name, textContains='HOME')

    @property
    def my_games(self):
        with step("Go MY GAMES tab"):
            return self.d.ui(packageName=self.package_name, textContains='MY GAMES')

    @property
    def all_games(self):
        with step("Go ALL GAMES tab"):
            return self.d.ui(packageName=self.package_name, textContains='ALL GAMES')


class StoreHomeScreen(StoreBase):

    @property
    def popular(self):
        self.d.ui(scrollable=True).scroll.to(packageName=self.package_name, resourceId='%s:id/popular' %
                                                                                       self.package_name)
        return self.d.ui(packageName=self.package_name, resourceId='%s:id/popular' % self.package_name)

    @property
    def editors_choice(self):
        self.d.ui(scrollable=True).scroll.to(packageName=self.package_name, resourceId='%s:id/editorsChoice' %
                                                                                       self.package_name)
        return self.d.ui(packageName=self.package_name, resourceId='%s:id/editorsChoice' % self.package_name)

    @property
    def new_games(self):
        self.d.ui(scrollable=True).scroll.to(packageName=self.package_name, resourceId='%s:id/newGames' %
                                                                                       self.package_name)
        return self.d.ui(packageName=self.package_name, resourceId='%s:id/newGames' % self.package_name)

    def popular_first(self):
        return self.popular.child(className='android.widget.Button').click()

    def editors_choice_first(self):
        return self.editors_choice.click()

    def new_games_first(self):
        return self.new_games.child(className='android.widget.Button').click()