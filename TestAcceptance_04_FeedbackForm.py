import pytest
from time import sleep

from AbstractTest import AbstractTest

step = pytest.allure.step


class TestAcceptanceGames(AbstractTest):

    def setup(self):
        AbstractTest.setup(self)
        self._run_app()

    def test_check_my_games_empty_on_first_start(self):
        """
        Open MyGames tab (first after install) and check that its empty
        """
        self.store_screen.action_bar.my_games.click.wait()
        assert self.d.ui(text='FIND GAMES').exists

    def install_first_one_game_from_all_games(self):
        """
        Install first games from All Games tab and check that its present in My Games tab
        """
        self.store_screen.action_bar.all_games.click.wait()
        # first game select
        self.d.ui(text='FREE', resourceId='%s:id/download' % self.store_screen.package_name).click.wait()
        with step("Download game"):
            downloading = self.d.ui(textContains='DOWNLOADING')
            assert downloading.exists
            timeout = 45
            while downloading.exists:
                timeout -= 1
                sleep(1)
                assert timeout, "Timeout for game download reached"
            assert self.d.ui(textContains='want to install').exists
        with step("Install game"):
            next_btn = self.d.ui(text='Next')
            if next_btn.exists:
                next_btn.click.wait()
            self.d.ui(text='Install').click.wait()
            sleep(10)
            assert self.d.ui(textContains='App installed').exists
            self.d.ui(text='Done', className='android.widget.Button').click.wait()
            assert self.current_app == self.store_screen.package_name
        with step("Check that PLAY button present"):
            assert self.d.ui(text='PLAY').exists

    def uninstall_first_one_game(self):
        """
        Uninstall game and check
        """
        self.store_screen.action_bar.my_games.click.wait()
        game = self.d.ui(resourceId='%s:id/full' % self.store_screen.package_name)
        games_before = game.count
        game.click.wait()
        assert self.d.ui(text='PLAY', className='android.widget.Button').exists
        self.store_screen.top_bar_more_options.click.wait()
        self.d.ui(text='UNINSTALL GAME').click.wait()
        self.d.ui(text='OK').click.wait()
        self.open_store_home_screen()
        self.store_screen.action_bar.my_games.click.wait()
        assert (games_before - 1) == game.count

    def test_install_game_from_all_games(self):
        self.install_first_one_game_from_all_games()

    def test_uninstall_game(self):
        self.uninstall_first_one_game()

    def test_install_two_games(self):
        self.store_screen.action_bar.my_games.click.wait()
        game = self.d.ui(resourceId='%s:id/full' % self.store_screen.package_name)
        game_before = game.count
        assert game.count == 0, "Should run with empty my games list"
        self.install_first_one_game_from_all_games()
        self.install_first_one_game_from_all_games()
        self.store_screen.action_bar.my_games.click.wait()
        assert game.count == game_before + 2

    def test_uninstall_two_games(self):
        self.store_screen.action_bar.my_games.click.wait()
        game = self.d.ui(resourceId='%s:id/full' % self.store_screen.package_name)
        assert game.count == 2, "Two games should be pre installed"
        with step("Uninstall first game"):
            self.uninstall_first_one_game()
        with step("Uninstall second game"):
            self.uninstall_first_one_game()

    def teardown(self):
        self._close_app()
        AbstractTest.teardown(self)
