import pytest

from AbstractTest import AbstractTest

step = pytest.allure.step


class TestAcceptanceBasicEnd(AbstractTest):

    def test_uninstall_app(self):
        """
        Uninstall store from device and check that package not present in common applications list
        """
        self.d.uninstall_app(self.app_info['name'])
        with step("Check that application uninstalled properly"):
            assert self.app_info['name'] not in self.d.system_app_list
        with step("Check browser feedback page opened"):
            browser_url = self.d.ui(resourceId='com.android.browser:id/url')
            assert 'android.games.playfree.org/support/uninstall' in browser_url.text
            self.d.stop_app('com.android.browser')

    def test_apk_icon_not_exist_on_launcher(self):
        """
        Check that store ico removed from launcher after uninstall
        """
        self.open_android_launcher()
        assert not self.store_launcher_ico
